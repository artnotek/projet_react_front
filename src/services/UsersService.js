const baseUrl = "http://localhost:3005";

class UsersService {
    /**
     * function to send a auth request to server
     * @param body
     * @returns {Promise<Response>}
     */
    static async auth(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };
        console.log(init);

        let call = await  fetch(`${baseUrl}/users/authentificate`, init);
        return call;
    }

    /**
     * function to send a server request to create a new user
     * @param body
     * @returns {Promise<Response>}
     */
    static async create(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/users`, init);
        return call;
    }

    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async list()
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                // "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        };

        let call = await  fetch(`${baseUrl}/users`, init);
        return call;
    }

    /**
     * function to send a request to get current user personnal data
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
                // "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        };

        let call = await  fetch(`${baseUrl}/users/${id}`, init);
        return call;
    }

    /**
     * function to send a update request for current user
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async update(id, body)
    {
        // console.log(body);
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        console.log(init);
        let call = await  fetch(`${baseUrl}/users/${id}`, init);
        return call;
    }
}

export default UsersService;
