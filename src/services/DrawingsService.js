const baseUrl = "http://localhost:3005";

class DrawingsService {
    /**
     * function to fetch informations about requested drawing
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };

        let call = await  fetch(`${baseUrl}/drawing/${id}`, init);
        return call;
    }

    /**
     * function to send to server a new drawing made by the SVG
     * @param body
     * @returns {Promise<Response>}
     */
    static async sendDrawing(body)
    {
        console.log(body);
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/drawing`, init);
        return call;
    }
}

export default DrawingsService;
