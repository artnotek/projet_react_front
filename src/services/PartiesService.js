const baseUrl = "http://localhost:3005";

class PartiesService {
    /**
     * function to send a new session to server
     * @param body
     * @returns {Promise<Response>}
     */
    static async create(body)
    {
        console.log(body);
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/newgame`, init);
        return call;
    }

    /**
     * function to request user to get into party
     * @param body
     * @returns {Promise<Response>}
     */
    static async joinSession(body)
        {
            // console.log(body);
            let init = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            };

            let call = await  fetch(`${baseUrl}/joingame`, init);
            return call;
        }

    /**
     * function to request all session currently available
     * @returns {Promise<Response>}
     */
    static async list()
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };

        let call = await  fetch(`${baseUrl}/joingame`, init);
        return call;
    }

    /**
     * function to request selected user personnal data
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };

        let call = await  fetch(`${baseUrl}/joingame/${id}`, init);
        return call;
    }

    /**
     * function to request to end current session
     * @param body
     * @returns {Promise<Response>}
     */
    static async endSession(body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/endparty`, init);
        return call;
    }

    /**
     * function to allow party owner request to hide user
     * @param body
     * @returns {Promise<Response>}
     */
    static async moderateUser(body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/hideselecteduser`, init);
        return call;
    }

    /**
     * function to allow party owner request to unhide user
     * @param body
     * @returns {Promise<Response>}
     */
    static async unModerateUser(body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/unhideselecteduser`, init);
        return call;
    }

    /**
     * function to allow party owner request to hide drawing
     * @param body
     * @returns {Promise<Response>}
     */
    static async moderateDrawing(body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/hideselecteddrawing`, init);
        return call;
    }

    /**
     * function to allow party owner request to unhide drawing
     * @param body
     * @returns {Promise<Response>}
     */
    static async unModerateDrawing(body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/unhideselecteddrawing`, init);
        return call;
    }
}

export default PartiesService;
