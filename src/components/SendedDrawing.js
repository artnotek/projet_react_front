import React, {Component} from "react";
import DrawingsService from "../services/DrawingsService";
import UsersService from "../services/UsersService";

class SendedDrawing  extends Component{
    state = {
        surname: '',
        link: '',
        isModerated: '',
        creatorId: '',
        userId: ''
    };

    async componentDidMount() {
        // console.log(this.props);
        // console.log(this.state);
        let {drawingLocationId, userId} = this.props.data;
        let drawingResponse = await DrawingsService.details(drawingLocationId);
        let userResponse = await UsersService.details(userId);
        if(drawingResponse.ok){
            let drawingData = await drawingResponse.json();
            this.setState({link: drawingData.drawing.link});
        }
        if(userResponse.ok){
            let userData = await userResponse.json();
            this.setState({surname: userData.user.surname});
        }
        this.setState({isModerated: this.props.data.isModerated, userId: this.props.data.userId, creatorId: this.props.creatorId});
        // console.log(this.state);
        // console.log(drawingLocationId);
        // let {id} = this.props.match.params;
        // console.log(this.state);
        console.log(this.props);
    }

    render() {
        let {surname, link} = this.state;
        let {_id} = this.props.data;
        return(
            <tr key={_id}>
                <td>{surname} </td>
                <td><img src={"data:image/svg+xml;base64, " + link} alt="dessin de l'user" className="img-responsive "/></td>
                <td> {(this.state.creatorId === localStorage.getItem('user_id'))
                        ? (this.state.isModerated === true)
                            ? <button className={'btn btn-info'}  onClick={() => this.props.unModerateDrawing()}><i className="fas fa-eye"></i></button>
                            : <button className={'btn btn-secondary'}  onClick={() => this.props.moderateDrawing()}><i className="fas fa-eye-slash"></i></button>
                        : (this.state.creatorId === localStorage.getItem('user_id'))
                        ? '(this.state.isModerated === true)'
                        : '' }</td>
            </tr>
        );
    }
}

export default SendedDrawing;
