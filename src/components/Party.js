import React, {Component} from "react";
import {Link} from "react-router-dom";

class Party extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        let {_id, name, theme} = this.props.data;

        return(
            <tr>

                <td>{name}</td>
                <td>{theme}</td>
                <td>
                    <Link to={`/details/${_id}`} className='btn btn-primary'><i className="fas fa-arrow-right">&nbsp;</i></Link>
                </td>
            </tr>
        );
    }
}

export default Party;