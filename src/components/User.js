import React, {Component} from "react";

class User extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }



    render(){
        let {firstname, lastname, email, surname, userRole} = this.props.data;


        //version daniel, on deconstruit
        return(
            <tr>
                <td>{firstname}</td>
                <td>{lastname}</td>
                <td>{email}</td>
                <td>{surname}</td>
                <td>{userRole}</td>
                <td><button className="btn btn-danger" onClick={(id) => this.props.delete(id)}>Supprimer</button></td>
            </tr>
        );
    }
}

export default User;