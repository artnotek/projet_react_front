import React, {Component} from "react";
import UsersService from "../services/UsersService";

class JoinedUser extends Component{
    constructor(props){
        super(props);
        this.state = {
            surname: '',
            isModerated: '',
            creatorId: '',
            userId: ''
        };
    }

    async componentDidMount() {
        let {userId} = this.props.data;
        let userResponse = await UsersService.details(userId);
        if(userResponse.ok){
            let userData = await userResponse.json();
            this.setState({surname: userData.user.surname});
        }
        this.setState({isModerated: this.props.data.isModerated, userId: this.props.data.userId, creatorId: this.props.creatorId});
        console.log(this.state);
    }

    render() {
        let {surname} = this.state;
        let {_id} = this.props.data;
        let masqued = (this.state.isModerated === true) ? 'info' : '';
        return(
            <tr key={_id} className={masqued}>
                <td className={masqued}>{surname}  &nbsp;  {(this.state.creatorId === this.state.userId) ? <i className="fas fa-crown text-warning"></i> : '' } </td>
                {/*<td>{(this.state.creatorId === localStorage.getItem('user_id')) ? <button className={'btn btn-info'} onClick={() => this.moderateUser(_id)}><i className="fas fa-eye-slash"></i></button> : '' }</td>*/}
                <td className={masqued}>{(this.state.creatorId === localStorage.getItem('user_id'))
                        ? (this.state.isModerated === true)
                            ? <button className={'btn btn-info'} onClick={() => this.props.unModerateUser()}><i className="fas fa-eye"></i></button>
                            : <button className={'btn btn-secondary'} onClick={() => this.props.moderateUser()}><i className="fas fa-eye-slash"></i></button>
                        : (this.state.creatorId === localStorage.getItem('user_id'))
                        ? ''
                        : ''
                        }
                </td>

            </tr>
        );
    }
}

export default JoinedUser;
