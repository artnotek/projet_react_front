import React, {Component} from "react";
import {Link} from "react-router-dom";

class Header extends Component {
    state = {
        isLogged: false
    }

    componentDidMount() {
        localStorage.getItem('token') && this.setState({isLogged: true});
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('user_id');
        window.location = "/";
        // this.props.history.push('/');
        // this.setState({'token'})
    }

    render() {
        let {isLogged} = this.state
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="/"><img src="/logo.png" alt="" className='logo'/></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        {
                            this.state.isLogged ?
                                (
                                    <ul className="navbar-nav mr-auto">
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/'}>Home</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/findgame'}>Rejoindre une session</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/newgame'}>Créer une session</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={`/account`}>Mon compte</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/'} onClick={this.logout}>Deconnexion</Link>
                                        </li>
                                    </ul>
                                )
                                : (
                                    <ul className="navbar-nav mr-auto">
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/'}>Home</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/login'}>Connexion</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to={'/register'}>Inscription</Link>
                                        </li>
                                    </ul>
                                )
                        }
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;
