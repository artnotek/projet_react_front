import React, {Component} from "react";
import UsersService from "../services/UsersService";
import User from "../components/User";
class Users extends Component{
    state = {
        title: "Utilisateurs enregistrés",

        users: []
    };

    async componentDidMount() {
        let response = await UsersService.list();
        if(response.ok){
            let data = await response.json();
            console.log(data);
            //this.state.users && this.state.users.map({user});
            this.setState({users: data.users});
            console.log(data.users);
            data.users.forEach(key => console.log(key));
            console.log('----------');
            console.log(this.state.users);
        }
    }

    // allow user to delete his account
    async deleteUser(id){
        let response = await UsersService.delete(id);
        if (response.ok){
            // // méthode 1
            // let data = await response.json();
            // this.setState()

            // méthode 2
            let users = this.state.users;
            let index = users.findIndex(users => users._id === id);
            users.splice(index, 1);

            this.setState({users: users});
        }
    }

    render() {

        return (
            <div className="container">
                <h1>{this.state.title}</h1>
                <table className="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Prenom</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Rôle</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.users.map(item => {
                            return(
                                <User data={item} btntext="Supprimer" delete={() => this.deleteUser(item._id)}/>
                            );
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Users;
