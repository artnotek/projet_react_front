import React, {Component} from "react";
import {Link} from "react-router-dom";


class Home extends Component{
    state = {
        isLogged: false
    };

    componentDidMount() {
        localStorage.getItem('token') && this.setState({isLogged: true});
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('user_id');
        window.location = "/";
    }

    render() {
        // let id = localStorage.getItem('user_id');
        return (
                <div className="container-fluid">
                    <div className="col-md-6">
                        <div className="row">
                            <h1>Créer et partager des dessins
                                suivant les thèmes que vous définissez</h1>

                                <h2>Et passer un moment de fun
                                pour réveiller l'artiste en vous</h2>
                            {
                                this.state.isLogged ? (
                                    <div>
                                        <Link to={`/account`}>Mon compte </Link>
                                        <hr/>
                                        <Link to={'/findgame'}>Rejoindre une session</Link>
                                        <Link to={'/newgame'}>Créer une session</Link>
                                        <Link to={'/'} className="nav-link" onClick={this.logout}>Deconnexion</Link>

                                    </div>
                                ) : (
                                    <div>
                                        <Link to={'/login'}>connexion</Link>
                                        <hr/>
                                        <Link to={'/register'}>inscription</Link>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    <div className="col-md-6 homeIllu">
                        &nbsp;
                        {/*<img src="illustration.png" alt=""/>*/}
                    </div>
                </div>
        );
    }

}

export default Home;
