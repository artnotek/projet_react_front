import React,{Component} from "react";
import PartiesService from "../services/PartiesService";
import Party from "../components/Party";

class FindGame extends Component{
    state = {
        parties:[]
    };

    async componentDidMount() {
        let response = await PartiesService.list();
        if(response.ok){
            let data = await response.json();
            this.setState({parties: data.parties});
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                    <tr>

                        <th>Créateur</th>
                        <th>thème</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.parties.map(item => {
                            return(
                                (item.isActive === false) ? item.continue : <Party data={item} key={item._id}  />
                            );
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default FindGame;