import React,{Component} from "react";
import DrawingsService from "../services/DrawingsService";
import Immutable from "immutable";
// import SvgSketchCanvas from "react-sketch-canvas";
// import { List, Map, mergeDeep } from 'immutable';
// import '../script/drawingCanvas';
class Drawing extends Component{
    // allow drawing component to inherit parent properties
    constructor(props) {
        super(props);

        this.state = {
            lines: new Immutable.List(),
            isDrawing: false
        };

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
    }

    // initiate mouse support for drawing
    componentDidMount() {
        document.addEventListener("mouseup", this.handleMouseUp);

    }

    // update mouse support
    componentWillUnmount() {
        document.removeEventListener("mouseup", this.handleMouseUp);
    }

    // track mouse click
    handleMouseDown(mouseEvent) {
        if (mouseEvent.button != 0) {
            return;
        }

        const point = this.relativeCoordinatesForEvent(mouseEvent);

        this.setState(prevState => ({
            lines: prevState.lines.push(new Immutable.List([point])),
            isDrawing: true
        }));
    }


    // mouse tracking
    handleMouseMove(mouseEvent) {
        if (!this.state.isDrawing) {
            return;
        }

        const point = this.relativeCoordinatesForEvent(mouseEvent);

        this.setState(prevState =>  ({
            lines: prevState.lines.updateIn([prevState.lines.size - 1], line => line.push(point))
        }));
    }

    // mouse tracking
    handleMouseUp() {
        this.setState({ isDrawing: false });
    }

    // mouse tracking
    relativeCoordinatesForEvent(mouseEvent) {
        const boundingRect = this.refs.drawArea.getBoundingClientRect();
        return new Immutable.Map({
            x: mouseEvent.clientX - boundingRect.left,
            y: mouseEvent.clientY - boundingRect.top,
        });
    }

    // after finishing, convert SVG to base64 img and send it
    handleChange(e){
        let SVGDomElement = document.getElementById("userDrawing");
        let serializedSVG = new XMLSerializer().serializeToString(SVGDomElement);
        let base64Data = window.btoa(serializedSVG);
        this.setState({
            link: base64Data,
            rawDrawingInfo: Date.now(),
            userId: localStorage.getItem('user_id'),
            partyId: localStorage.getItem('partyId')
        });
    }

    // send current drawing to server
    async sendDrawing(e){
        e.preventDefault();

        // console.log(e);
        let response = await DrawingsService.sendDrawing(this.state);
        if(response.ok){
            console.log(response);
            this.props.history.push(`/details/${this.state.partyId}`);
        }else{
            console.log(response);
        }
        // console.log(this.state);
    }


    render() {
        function Drawingg({ lines }) {
            return (
                <svg className="drawing" id="userDrawing">
                    {lines.map((line, index) => (
                        <DrawingLine key={index} line={line} />
                    ))}
                </svg>
            );
        }

        function DrawingLine({ line }) {
            const pathData = "M " +
                line
                    .map(p => {
                        return `${p.get('x')} ${p.get('y')}`;
                    })
                    .join(" L ");

            return <path className="path" d={pathData} />;
        }
        return (
            <div className="container">
                <br/>
                <div className="row justify-content-center">
                    <form onSubmit={(e) => this.sendDrawing(e)}>
                        <div className="row">
                            <input type="hidden" value={localStorage.getItem('user_id')}/>
                            <input type="hidden" value={localStorage.getItem('partyId')}/>
                            <div
                                className="drawArea"
                                ref="drawArea"
                                onMouseDown={this.handleMouseDown}
                                onMouseMove={this.handleMouseMove}
                            >
                                <Drawingg lines={this.state.lines} />
                            </div>
                        </div>
                        <br/>
                        <div className="row justify-content-center">
                            <button type={"submit"} onClick={(e) => this.handleChange(e)} className="btn btn-info">end</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Drawing;
