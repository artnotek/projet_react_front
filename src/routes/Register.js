import UsersService from "../services/UsersService";
import React, {Component} from "react";

class Register extends Component{
    state = {
        firstname: "",
        lastname: "",
        email: "",
        surname: "",
        password: "",
        userRole: ""
    };

    // listerner to input form data
    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    // send register informations request to server
    async submit(e){
        e.preventDefault();
        console.log('user bien crée');
        let response = await UsersService.create(this.state);
        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.props.history.push('/');

        }else{
            console.log(response);
        }
    }

    render() {
        let {firstname, lastname, surname, email, password} = this.state;
        return (
            <div className="container">
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Prénom</label>
                        <input type="text" id={'firstname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={firstname}/>
                    </div>
                    <div className="form-group">
                        <label>Nom</label>
                        <input type="text" id={'lastname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={lastname}/>
                    </div>
                    <div className="form-group">
                        <label>Pseudo</label>
                        <input type="text" id={'surname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={surname}/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input type="text" id={'email'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={email}/>
                    </div>
                    <div className="form-group">
                        <label>Mot de passe</label>
                        <input type="password" id={'password'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={password}/>
                    </div>
                    <button type={"submit"} className={'btn btn-primary'}>Inscription</button>
                </form>
            </div>
        );
    }
}

export default Register;
